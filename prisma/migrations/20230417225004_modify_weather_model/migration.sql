/*
  Warnings:

  - You are about to drop the column `rainfallPercent` on the `Weather` table. All the data in the column will be lost.
  - You are about to drop the column `temperature` on the `Weather` table. All the data in the column will be lost.
  - Added the required column `maxTemp` to the `Weather` table without a default value. This is not possible if the table is not empty.
  - Added the required column `meanTemp` to the `Weather` table without a default value. This is not possible if the table is not empty.
  - Added the required column `medianTemp` to the `Weather` table without a default value. This is not possible if the table is not empty.
  - Added the required column `minTemp` to the `Weather` table without a default value. This is not possible if the table is not empty.
  - Added the required column `rainfallChance` to the `Weather` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Weather" DROP COLUMN "rainfallPercent",
DROP COLUMN "temperature",
ADD COLUMN     "maxTemp" INTEGER NOT NULL,
ADD COLUMN     "meanTemp" INTEGER NOT NULL,
ADD COLUMN     "medianTemp" INTEGER NOT NULL,
ADD COLUMN     "minTemp" INTEGER NOT NULL,
ADD COLUMN     "rainfallChance" INTEGER NOT NULL;
