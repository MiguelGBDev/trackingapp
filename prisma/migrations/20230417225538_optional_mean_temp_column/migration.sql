/*
  Warnings:

  - Made the column `medianTemp` on table `Weather` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Weather" ALTER COLUMN "meanTemp" DROP NOT NULL,
ALTER COLUMN "medianTemp" SET NOT NULL;
