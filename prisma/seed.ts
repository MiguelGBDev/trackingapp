import { PrismaClient } from '@prisma/client'
import * as csv from 'csvtojson'
import * as dotenv from 'dotenv'
dotenv.config()

import { ExternalWeatherService } from '../src/utils/weatherInfo/externalWeatherService'
import { roundFloat } from '../src/utils/roundFloat/roundFloat'

const GPS_FILE = `${__dirname}/../infra/files/gps.csv`
const TRACKING_FILE = `${__dirname}/../infra/files/trackings.csv`

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const { COORDINATES_DECIMAL_COUNTER } = process.env!
const DECIMAL_COUNTER = parseInt(COORDINATES_DECIMAL_COUNTER)

const prisma = new PrismaClient()

async function main() {
  const gpsRows = await csv({ noheader: true }).fromFile(GPS_FILE)
  const gpsResult = composeData(gpsRows)
  await prisma.location.createMany({ data: gpsResult })

  const trackingRows = await csv({ noheader: true }).fromFile(TRACKING_FILE)
  const trackingResult = composeData(trackingRows, true)
  await prisma.tracking.createMany({ data: trackingResult })

  const weatherResult = await new ExternalWeatherService().getWeatherInfo(gpsResult)
  await prisma.weather.createMany({ data: weatherResult })
}

const composeData = (rows, type = false) => {
  const resultRows = []

  for (const row of rows) {
    if (!type) {
      // GPS FILE
      resultRows.push({
        id: row.field1,
        latitude: roundFloat(row.field2, DECIMAL_COUNTER),
        longitude: roundFloat(row.field3, DECIMAL_COUNTER)
      })
    } else {
      // TRACKING FILE
      resultRows.push({
        trackingNumber: row.field1,
        locationId: row.field2,
        pickupDate: parseDatetime(row.field3)
      })
    }
  }

  return resultRows
}

const parseDatetime = (rawDate: string): Date => {
  const year = rawDate.slice(0, 4)
  const month = rawDate.slice(4, 6)
  const day = rawDate.slice(6, 8)
  const hour = rawDate.slice(8, 10)
  const minute = rawDate.slice(10, 12)
  const second = rawDate.slice(-2)

  return new Date(`${year}-${month}-${day}T${hour}:${minute}:${second}`)
}

main()
  .catch(e => {
    console.error(e)
    process.exit(1)
  })
  .finally(async () => {
    await prisma.$disconnect()
  })
