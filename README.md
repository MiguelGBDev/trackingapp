## Proposal

Required functionalities:

- [x] A server
  - [x] that allows two calls
    - [x] tracking number → pickup location, pickup date and time, predicted weather
    - [x] gps coordinates, a date and time → predicted weather at that location
  - [x] that fetches data from the database if available
  - [x] that fetches data from from a weather api if not available
  - [x] that periodically refreshes data from a weather api
- [x] A database
  - [x] That stores the data
- [x] A setup script
  - [x] that parses and processes the provided CSV files' content
- [x] Documentation
  - [x] Of how to setup and start the system
  - [x] Of how to call your API
  - [x] Of how to add API keys for the weather app

## State of art

The proposal is implemented using some modern and advanced technologies to face the real problem.

- There are three entities: Tracking, Weather and Location
- The server provides a starting script to populate the DB from CSVs data and third party service.
- Weather info comes from a third party API.
- Server udpates peridiocally the weather calling to the third party service.
- The period is defined by an environment variable and allows values between 5 minutes and 2 hours.
- If the weather is not stored on the database, the server calls to the third party service to get it, and store it for next calls.
- Third party service required to have a token to interact with it. For free accounts, the info allowed is limited.
- The server consumes two endpoints from the 3rd party service: current time and historic (limited to short periods).
- Mean required by the statement is not available for free accounts.
- Location coordinates are rounded by a environmental variable and it can be changed to increase/decrease precision.

![IMAGE_DESCRIPTION](flows.png)

## Technologies applied

- [PostgreSQL](https://www.postgresql.org/)
- [Docker](https://www.docker.com/) & [Docker desktop](https://www.docker.com/products/docker-desktop/)
- [Prisma ORM](https://www.prisma.io/)
- [Nodejs](https://nodejs.org/en)
- [Nestjs framework](https://github.com/nestjs/nest)
- [Postman](https://www.postman.com/)
- [DBeaver](https://dbeaver.io/)
- [Typescript](https://www.typescriptlang.org/)

## Nest features

- Modules
- Services
- Controllers
- ScheduleModule

## Prisma features

- Migrations (Create the full structure of the database in an iterative way)
- Seeds (Populate the database with initial data)

## Tech implementation

### Why Nestjs?

Nestjs framework provides highly advanced features and design patterns in an easy way.
It provides a handy way to use dependency injection with a container design pattern.
It uses decorator features.

The main reason why I choose Nest to make this project is I consider it a good opportunity to learn about it and test if it speedup up the developing (IMHO, it does!).

### Why Prisma?

Prisma ORM provides protection to SQL Injection, and an easy middleware to make queries in a programmatic way.
It provides as well a useful database seed.
Prisma also includes migrations feature to create/modify the final database structure in an incremental mode, a very handy way to recover from errors in structure definition.

### Database structure

We have three entities: Tracking, Location and Weather.

Location entitiy in on the top, and Tracking and Weather are related to it.

![IMAGE_DESCRIPTION](database.png)

### Service arquitecture

The architecture of the API follows the DDD concepts for domain entities, use cases and business logic isolated.
There is an implementation for the design pattern repository to create an extra layer to communicate with the database to ingest or receive data.

So, in the end, we have controllers for the desired endpoints, followed by services that coordinates the business rules, and three repositories, one per Entity.
An extra isolated service is implemented to ingest data periodically.

![IMAGE_DESCRIPTION](arquitecture.png)

## Explanations and comments

### Third party weather service use

The service requires a token for a registered account to allow the calls. A token for a free account is provided and stored at the OPENWEATHERAPP_TOKEN environment variable.
Its possible to replace it with another one. No more configuration is required.

### Seed script

The initial data ingest reads provided CSV files to populate the DB for locations and tracking entities.
After those data are stored, it calls to the third party weather service to retrieve weather info for the location's coordinates and store it on the DB as well.
Precision for coordinates is given by the COORDINATES_DECIMAL_COUNTER environment variable.

### Task cronjob

There is an additional script that update the weather info for the current time.
It calls periodically the third party weather service to retrieve the weathere and store the new data on the DB.
The period is defined by the REFRESH_WEATHER_PERIOD_MINUTES env var.

It will be the same mechanism as the seed, creating a value for the current moment, and another one for the same moment in the previous year the next day.
At this way, the result of calling the endpoint can be modified over time.

Once the script runs, it prints at the console a brief comment:

```
[Mon Apr 24 2023 18:50:01 GMT+0200 (hora de verano de Europa central)] - Weather info updated
```

## Requirements

- npm
- nodejs
- docker

Open a terminal and type:

```bash
$ npm install
```

With this command all interal dependencies for the project will be added.

## Prepare the environment

### How to add API Keys for the weather app

Move to .env file at the root folder and replace the OPENWEATHERAPP_TOKEN env var with the desired one.

### Build database cointainer

First, we will create our database in a docker container and deploying it:

```bash
$ nmp run infra:up
```

### Create database structure and insert initial data

The second and the third step is to create the data structure and fill the DB with the initial mocked data. We will accomplish it with a single command:

```bash
$ npm run db:reset
```

CAUTION: If you ran it previously and you use it another time, previous data will be removed!

## Running the app

Use any of the commands below to start the app (dev command is recommended):

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

With these commands, the app starts to listen to requests from the outside. The cronjob for mock data ingest should start running as well.

### How to call the API

Import the Postman collection include it in the source folder in a Postman client and send the predefined requests to check the responses.

There are two endpoints:

- GET Tracking/trackingNumber: Provides pickup location, pickup datetime, and predicted weather.
- GET location?latitude=LATITUDE_COORDINATES&longitude=LONGITUDE_COORDINATES&date=DESIRED_DATE (ISO_FORMAT): Provides predicted weather at that location

## Test

The testing tool used is Jest.

There are some tests included to show the use of mocks, spies, and Jest features.
I preferred to invest more time in the project arquitecture, so I didn't implement a full suite of test.
Even so, the project included some advanced test funcionalities as error catching, mocks, and spies.

The test suite covered all the possible input casses for the endpoints.

To run the test suite:

```bash
$ npm test
```
