import { createMock } from '@golevelup/ts-jest'

import { WeatherService } from './weather.service'
import { WeatherRepository } from '../repositories/weatherRepository'
import { ExternalWeatherService } from '../utils/weatherInfo/externalWeatherService'
import { Location } from '../location/entities/location.entity'
import { Weather } from './entities/weather.entity'

const LOCATION_MOCK_INPUT: Location = { id: 'locationId', latitude: 4.444, longitude: 4.444 }
const WEATHER_MOCK_RESULT = {} as Weather

describe('WeatherService', () => {
  let service: WeatherService
  let weatherRepository: WeatherRepository
  let externalWeatherService: ExternalWeatherService

  beforeEach(async () => {
    weatherRepository = createMock<WeatherRepository>()
    externalWeatherService = createMock<ExternalWeatherService>()
    service = new WeatherService(weatherRepository, externalWeatherService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  describe('getByLocationId', () => {
    let weatherRepoGetByLocationIdSpy: jest.SpyInstance
    let weatherRepoSaveSpy: jest.SpyInstance
    let extWeatherSvcGetWeatherInfoSpy: jest.SpyInstance

    beforeEach(() => {
      weatherRepoGetByLocationIdSpy = jest.spyOn(weatherRepository, 'getByLocationId')
      weatherRepoSaveSpy = jest.spyOn(weatherRepository, 'save')
      extWeatherSvcGetWeatherInfoSpy = jest.spyOn(externalWeatherService, 'getWeatherInfo')
    })

    afterEach(() => {
      weatherRepoGetByLocationIdSpy.mockClear()
      weatherRepoSaveSpy.mockClear()
      extWeatherSvcGetWeatherInfoSpy.mockClear()
    })

    it('should return the expect weather if the data is available on DB', async () => {
      weatherRepoGetByLocationIdSpy.mockImplementation(() => {
        return WEATHER_MOCK_RESULT
      })

      const response = await service.getByLocationId(LOCATION_MOCK_INPUT)
      expect(response).toBe(WEATHER_MOCK_RESULT)
      expect(weatherRepoGetByLocationIdSpy).toHaveBeenCalled()
      expect(extWeatherSvcGetWeatherInfoSpy).not.toHaveBeenCalled()
      expect(weatherRepoSaveSpy).not.toHaveBeenCalled()
    })

    it('should return the weather from the external service if the data is NOT available on DB', async () => {
      weatherRepoGetByLocationIdSpy.mockImplementation(() => {
        return null
      })

      extWeatherSvcGetWeatherInfoSpy.mockImplementation(() => {
        return [WEATHER_MOCK_RESULT]
      })

      const response = await service.getByLocationId(LOCATION_MOCK_INPUT)
      expect(response).toBe(WEATHER_MOCK_RESULT)
      expect(weatherRepoGetByLocationIdSpy).toHaveBeenCalled()
      expect(extWeatherSvcGetWeatherInfoSpy).toHaveBeenCalled()
      expect(weatherRepoSaveSpy).toHaveBeenCalled()
    })

    it('should throw an error if it can not provide weather info from DB nor from Service', async () => {
      weatherRepoGetByLocationIdSpy.mockImplementation(() => {
        return null
      })

      extWeatherSvcGetWeatherInfoSpy.mockImplementation(() => {
        return null
      })

      await expect(service.getByLocationId(LOCATION_MOCK_INPUT)).rejects.toThrow(
        `Unable to find weather information for the tracking location`
      )
      expect(weatherRepoGetByLocationIdSpy).toHaveBeenCalled()
      expect(extWeatherSvcGetWeatherInfoSpy).toHaveBeenCalled()
      expect(weatherRepoSaveSpy).not.toHaveBeenCalled()
    })
  })
})
