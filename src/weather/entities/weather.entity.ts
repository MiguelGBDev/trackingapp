export class Weather {
  rainfallChance: number
  minTemp: number
  maxTemp: number
  meanTemp: number // mean isn't covered by free subscription
  medianTemp: number
  humidity: number
  locationId: string
}
