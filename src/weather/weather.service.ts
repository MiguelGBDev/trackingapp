import { Injectable } from '@nestjs/common'

import { WeatherRepository } from '../repositories/weatherRepository'
import { ExternalWeatherService } from '../utils/weatherInfo/externalWeatherService'
import { Location } from 'src/location/entities/location.entity'
import { Weather } from './entities/weather.entity'

@Injectable()
export class WeatherService {
  constructor(
    private readonly weatherRepository: WeatherRepository,
    private readonly externalWeatherService: ExternalWeatherService
  ) {}

  async getByLocationId(location: Location) {
    let weatherInfoSvc: Weather
    const weatherInfoDb = await this.weatherRepository.getByLocationId(location.id)

    if (!weatherInfoDb) {
      weatherInfoSvc = await this.externalWeatherService.getWeatherInfo([location])?.[0]
      if (!weatherInfoSvc) {
        throw new Error(`Unable to find weather information for the tracking location`)
      }

      await this.weatherRepository.save(weatherInfoSvc)
    }

    return weatherInfoDb || weatherInfoSvc
  }
}
