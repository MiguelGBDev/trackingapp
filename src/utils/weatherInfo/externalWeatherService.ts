import { ForbiddenException, Injectable } from '@nestjs/common'
import * as dotenv from 'dotenv'

import { Location } from '../../location/entities/location.entity'
import { Weather } from '../../weather/entities/weather.entity'

const WEATHER_API_URL = `https://api.openweathermap.org/data/2.5/{{querytype}}?lat={{latitude}}&lon={{longitude}}&appid=`

@Injectable()
export class ExternalWeatherService {
  WEATHER_ENDPOINT: string
  FORECAST_ENDPOINT: string

  constructor() {
    dotenv.config()
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const { OPENWEATHERAPP_TOKEN } = process.env!
    const source = `${WEATHER_API_URL}${OPENWEATHERAPP_TOKEN}`
    this.WEATHER_ENDPOINT = source.replace('{{querytype}}', 'weather')
    this.FORECAST_ENDPOINT = source.replace('{{querytype}}', 'forecast')
  }

  public async getWeatherInfo(locations: Location[]): Promise<Weather[]> {
    const promisesWeather = []
    const promisesForecast = []

    // Fill queryParams and store calls
    for (const loc of locations) {
      const [weatherUrl, forecastUrl] = this.setLinskWeatherApi(loc.latitude, loc.longitude)
      promisesWeather.push(fetch(weatherUrl))
      promisesForecast.push(fetch(forecastUrl))
    }

    // API calls promises waterfall
    const resultsWeather = await Promise.all(promisesWeather)
    const resultsForecast = await Promise.all(promisesForecast)
    const promisesWeatherJson = []
    const promisesForecastJson = []

    for (let i = 0; i < resultsWeather.length; i++) {
      promisesWeatherJson.push(resultsWeather[i].json())
      promisesForecastJson.push(resultsForecast[i].json())
    }

    // to json promises waterfall. Yes, sadly .json() method is async :(
    const weatherResults = await Promise.all(promisesWeatherJson)
    const forecastResults = await Promise.all(promisesForecastJson)

    const weatherData = []
    // Promise.all preserve order, so we can rely on the locations's id
    for (let i = 0; i < weatherResults.length; i++) {
      try {
        weatherData.push(this.parseWeather(weatherResults[i], forecastResults[i], locations[i].id))
      } catch (error) {
        const message = weatherResults[i].message || forecastResults[i].message
        if (weatherResults[i].cod === 401 || forecastResults[i].cod === 401) {
          throw new ForbiddenException(message)
        }

        throw new Error('Unable to receive weather info from external API')
      }
    }

    return weatherData
  }

  private parseWeather(rawWeather, rawForecast, locationId): Weather {
    return {
      rainfallChance: rawForecast?.list[0]?.pop,
      minTemp: rawWeather?.main.temp_min,
      maxTemp: rawWeather?.main.temp_max,
      humidity: rawWeather?.main.humidity,
      medianTemp: rawWeather?.main.temp,
      meanTemp: null, //mean is not provided by openWeatherAPI free account :(
      locationId
    }
  }

  private setLinskWeatherApi(lat, lon) {
    const weatherUrl = this.WEATHER_ENDPOINT.replace('{{latitude}}', lat).replace('{{longitude}}', lon)
    const forecastUrl = this.FORECAST_ENDPOINT.replace('{{latitude}}', lat).replace('{{longitude}}', lon)

    return [weatherUrl, forecastUrl]
  }
}
