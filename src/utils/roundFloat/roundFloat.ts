export const roundFloat = (stringyNumber: string, decimalCounter): number => {
  const number = parseFloat(stringyNumber)
  return Math.round(number * Math.pow(10, decimalCounter)) / Math.pow(10, decimalCounter)
}
