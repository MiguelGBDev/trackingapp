import { Module } from '@nestjs/common'
import { TrackingService } from './tracking.service'
import { TrackingController } from './tracking.controller'
import { TrackingRepository } from 'src/repositories/trackingRepository'
import { LocationRepository } from 'src/repositories/locationRepository'
import { WeatherService } from 'src/weather/weather.service'
import { PrismaClient } from '@prisma/client'
import { WeatherRepository } from 'src/repositories/weatherRepository'
import { ExternalWeatherService } from 'src/utils/weatherInfo/externalWeatherService'

@Module({
  controllers: [TrackingController],
  providers: [
    TrackingService,
    TrackingRepository,
    LocationRepository,
    WeatherService,
    PrismaClient,
    WeatherRepository,
    ExternalWeatherService
  ]
})
export class TrackingModule {}
