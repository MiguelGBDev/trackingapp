import { Injectable, NotFoundException } from '@nestjs/common'
import { LocationRepository } from '../repositories/locationRepository'
import { TrackingRepository } from '../repositories/trackingRepository'
import { WeatherService } from '../weather/weather.service'
import { Tracking } from './entities/tracking.entity'

@Injectable()
export class TrackingService {
  constructor(
    private readonly trackingRepository: TrackingRepository,
    private readonly locationRepository: LocationRepository,
    private readonly weatherService: WeatherService
  ) {}

  // Given a tracking number, return the pickup location,
  // expected pickup date and time, and predicted weather at that location
  async findOne(trackingNumber: string) {
    const tracking = await this.trackingRepository.getByTrackingNumber(trackingNumber)
    if (!tracking) {
      throw new NotFoundException(`Article with trackingNumber: ${trackingNumber} does not exist.`)
    }

    const location = await this.locationRepository.getById(tracking.locationId)
    if (!location) {
      throw new NotFoundException('Location not found, unable to get its coordinates')
    }

    const weatherInfo = await this.weatherService.getByLocationId(location)

    const trackingInfo: Tracking = { ...location, ...weatherInfo }

    return trackingInfo
  }
}
