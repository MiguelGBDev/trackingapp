import { createMock } from '@golevelup/ts-jest'

import { TrackingService } from './tracking.service'
import { LocationRepository } from 'src/repositories/locationRepository'
import { TrackingRepository } from 'src/repositories/trackingRepository'
import { WeatherService } from 'src/weather/weather.service'

const VALID_TRACKING_NUMBER = 'validTrackingNumber'
const INVALID_TRACKING_NUMBER = 'invalidTrackingNumber'
const OUTPUT_DATE = new Date()

describe('TrackingService', () => {
  let service: TrackingService
  let trackingRepository: TrackingRepository
  let locationRepository: LocationRepository
  let weatherService: WeatherService

  beforeEach(() => {
    trackingRepository = createMock<TrackingRepository>()
    locationRepository = createMock<LocationRepository>()
    weatherService = createMock<WeatherService>()

    service = new TrackingService(trackingRepository, locationRepository, weatherService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  describe('findOne', () => {
    let getByTrackingNumberSpy: jest.SpyInstance
    let locationGetById: jest.SpyInstance
    let weatherGetByLocationId: jest.SpyInstance

    beforeEach(() => {
      getByTrackingNumberSpy = jest.spyOn(trackingRepository, 'getByTrackingNumber')
      locationGetById = jest.spyOn(locationRepository, 'getById')
      weatherGetByLocationId = jest.spyOn(weatherService, 'getByLocationId')
    })

    afterEach(() => {
      getByTrackingNumberSpy.mockClear()
    })

    it('should return the tracking info for a valid input', async () => {
      getByTrackingNumberSpy.mockImplementation(async trackingNumber => {
        return {
          trackingNumber,
          pickupDate: new Date(),
          locationId: 'validLocationId'
        }
      })
      locationGetById.mockImplementation(async id => {
        return {
          id,
          latitude: 'latitude1',
          longitude: 'longitude1'
        }
      })
      weatherGetByLocationId.mockImplementation(async location => {
        return {
          id: 'weatherId',
          rainfallChance: 0,
          minTemp: 0,
          maxTemp: 0,
          meanTemp: null,
          medianTemp: 0,
          humidity: 0,
          createdAt: OUTPUT_DATE,
          locationId: location.id
        }
      })

      const response = await service.findOne(VALID_TRACKING_NUMBER)

      expect(response).toEqual(
        expect.objectContaining({
          id: 'weatherId',
          latitude: 'latitude1',
          longitude: 'longitude1',
          rainfallChance: 0,
          minTemp: 0,
          maxTemp: 0,
          meanTemp: null,
          medianTemp: 0,
          humidity: 0,
          createdAt: OUTPUT_DATE,
          locationId: 'validLocationId'
        })
      )
    })

    it('should throw error for an invalid input', async () => {
      getByTrackingNumberSpy.mockImplementation(async () => null)

      await expect(service.findOne(INVALID_TRACKING_NUMBER)).rejects.toThrow(
        `Article with trackingNumber: ${INVALID_TRACKING_NUMBER} does not exist.`
      )
    })

    it('should throw error if the trackingNumber has no a location', async () => {
      getByTrackingNumberSpy.mockImplementation(async trackingNumber => {
        return {
          trackingNumber,
          pickupDate: new Date(),
          locationId: 'validLocationId'
        }
      })
      locationGetById.mockImplementation(async () => null)

      await expect(service.findOne(INVALID_TRACKING_NUMBER)).rejects.toThrow(
        'Location not found, unable to get its coordinates'
      )
    })
  })
})
