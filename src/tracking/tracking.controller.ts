import { BadRequestException, Controller, Get, Param } from '@nestjs/common'

import { TrackingService } from './tracking.service'

@Controller('tracking')
export class TrackingController {
  constructor(private readonly trackingService: TrackingService) {}

  @Get(':trackingNumber')
  async findOne(@Param('trackingNumber') trackingNumber: string) {
    if (!trackingNumber) {
      throw new BadRequestException(`Wrong trackingNumber input`)
    }
    return this.trackingService.findOne(trackingNumber)
  }
}
