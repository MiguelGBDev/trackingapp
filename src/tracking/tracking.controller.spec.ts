import { createMock } from '@golevelup/ts-jest'

import { TrackingController } from './tracking.controller'
import { TrackingService } from './tracking.service'

const VALID_INPUT = 'ValidTrackingNumber'
const SERVICE_OBJECT_RESPONSE = {}

describe('TrackingController', () => {
  let controller: TrackingController
  let trackingService: TrackingService

  beforeEach(() => {
    trackingService = createMock<TrackingService>()
    controller = new TrackingController(trackingService)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  describe('GET findOne', () => {
    let serviceFindOneSpy: jest.SpyInstance

    beforeEach(() => {
      serviceFindOneSpy = jest.spyOn(trackingService, 'findOne')
    })

    afterEach(() => {
      serviceFindOneSpy.mockClear()
    })

    it('Should return a response for a correct input', async () => {
      serviceFindOneSpy.mockImplementation(() => {
        return SERVICE_OBJECT_RESPONSE
      })
      const response = await controller.findOne(VALID_INPUT)
      expect(response).toBe(SERVICE_OBJECT_RESPONSE)
    })

    it('Should throw error for undefined input', async () => {
      await expect(controller.findOne(undefined)).rejects.toThrow(`Wrong trackingNumber input`)
    })
  })
})
