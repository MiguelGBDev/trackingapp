import { Module } from '@nestjs/common'
import { ScheduleModule } from '@nestjs/schedule'
import { PrismaClient } from '@prisma/client'

import { AppController } from './app.controller'
import { AppService } from './app.service'
import { TrackingModule } from './tracking/tracking.module'
import { LocationModule } from './location/location.module'
import { WeatherService } from './weather/weather.service'
import { WeatherRepository } from './repositories/weatherRepository'
import { ExternalWeatherService } from './utils/weatherInfo/externalWeatherService'
import { LocationRepository } from './repositories/locationRepository'
import { CronWeatherService } from './cron-weather/cron-weather.service'

@Module({
  imports: [TrackingModule, LocationModule, ScheduleModule.forRoot()],
  controllers: [AppController],
  providers: [
    AppService,
    WeatherService,
    ExternalWeatherService,
    WeatherRepository,
    LocationRepository,
    PrismaClient,
    CronWeatherService
  ]
})
export class AppModule {}
