import { createMock } from '@golevelup/ts-jest'

import { LocationService } from './location.service'
import { LocationRepository } from '../repositories/locationRepository'
import { WeatherRepository } from '../repositories/weatherRepository'

const COORDINATE_NUMBER = 4.444
const WEATHER_OBJECT_RESPONSE = {}

describe('LocationService', () => {
  let service: LocationService
  let locationRepository: LocationRepository
  let weatherRepository: WeatherRepository

  beforeEach(async () => {
    locationRepository = createMock<LocationRepository>()
    weatherRepository = createMock<WeatherRepository>()
    service = new LocationService(locationRepository, weatherRepository)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  describe('findOne', () => {
    let locRepoGetByCoordinatesSpy: jest.SpyInstance
    let weatherRepoGetLastByLocationIdAndDateSpy: jest.SpyInstance

    beforeEach(() => {
      locRepoGetByCoordinatesSpy = jest.spyOn(locationRepository, 'getByCoordinates')
      weatherRepoGetLastByLocationIdAndDateSpy = jest.spyOn(weatherRepository, 'getLastByLocationIdAndDate')
    })

    afterEach(() => {
      locRepoGetByCoordinatesSpy.mockClear()
      weatherRepoGetLastByLocationIdAndDateSpy.mockClear()
    })

    it('should return the expect weather for the location and time', async () => {
      locRepoGetByCoordinatesSpy.mockImplementation(() => {
        return { id: 'locationId' }
      })

      weatherRepoGetLastByLocationIdAndDateSpy.mockImplementation(() => {
        return WEATHER_OBJECT_RESPONSE
      })

      const response = await service.findOne(COORDINATE_NUMBER, COORDINATE_NUMBER, new Date())
      expect(response).toBe(WEATHER_OBJECT_RESPONSE)
      expect(locRepoGetByCoordinatesSpy).toHaveBeenCalled()
      expect(weatherRepoGetLastByLocationIdAndDateSpy).toHaveBeenCalled()
    })

    it('should throw error if location is not found', async () => {
      locRepoGetByCoordinatesSpy.mockImplementation(() => null)

      await expect(service.findOne(COORDINATE_NUMBER, COORDINATE_NUMBER, new Date())).rejects.toThrow(
        'Location not found, unable to get its coordinates'
      )
      expect(locRepoGetByCoordinatesSpy).toHaveBeenCalled()
      expect(weatherRepoGetLastByLocationIdAndDateSpy).not.toHaveBeenCalled()
    })

    it('should throw error if weather for the location is not found', async () => {
      locRepoGetByCoordinatesSpy.mockImplementation(() => {
        return { id: 'locationId' }
      })

      weatherRepoGetLastByLocationIdAndDateSpy.mockImplementation(() => {
        return null
      })

      await expect(service.findOne(COORDINATE_NUMBER, COORDINATE_NUMBER, new Date())).rejects.toThrow(
        'Unable to provide weather info for the location'
      )
      expect(locRepoGetByCoordinatesSpy).toHaveBeenCalled()
      expect(weatherRepoGetLastByLocationIdAndDateSpy).toHaveBeenCalled()
    })
  })
})
