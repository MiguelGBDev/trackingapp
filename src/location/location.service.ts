import { Injectable, NotFoundException } from '@nestjs/common'

import { LocationRepository } from '../repositories/locationRepository'
import { WeatherRepository } from '../repositories/weatherRepository'
import { Weather } from '../weather/entities/weather.entity'

@Injectable()
export class LocationService {
  constructor(
    private readonly locationRepository: LocationRepository,
    private readonly weatherRepository: WeatherRepository
  ) {}

  // Given gps coordinates and a date and time, return the predicted weather at that location
  async findOne(latitude: number, longitude: number, date: Date): Promise<Weather> {
    const location = await this.locationRepository.getByCoordinates(latitude, longitude)
    if (!location) {
      throw new NotFoundException('Location not found, unable to get its coordinates')
    }

    const lastWeather = await this.weatherRepository.getLastByLocationIdAndDate(location.id, date)

    // Here I would implement a call the openWeatherAPI if I haven't a near response
    // but historic is not allowed for free accounts

    if (!lastWeather) {
      throw new NotFoundException('Unable to provide weather info for the location')
    }

    return lastWeather
  }
}
