import { Module } from '@nestjs/common'
import { LocationService } from './location.service'
import { LocationController } from './location.controller'
import { LocationRepository } from 'src/repositories/locationRepository'
import { PrismaClient } from '@prisma/client'
import { WeatherRepository } from 'src/repositories/weatherRepository'

@Module({
  controllers: [LocationController],
  providers: [LocationService, LocationRepository, PrismaClient, WeatherRepository]
})
export class LocationModule {}
