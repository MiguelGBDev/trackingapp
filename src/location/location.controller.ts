import { BadRequestException, Controller, Get, Query } from '@nestjs/common'

import { LocationService } from './location.service'
import { Weather } from 'src/weather/entities/weather.entity'

@Controller('location')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @Get()
  async getPredictedWeather(@Query() query: { latitude: string; longitude: string; date: string }): Promise<Weather> {
    const { latitude, longitude, date: dateInput } = query

    const floatLatitude = parseFloat(latitude)
    const floatLongitude = parseFloat(longitude)

    if (isNaN(floatLatitude) || isNaN(floatLongitude)) {
      throw new BadRequestException('Bad request', { cause: new Error(), description: 'Incorrect coordinates format' })
    }

    const formattedDate = new Date(dateInput)
    if (!(formattedDate instanceof Date) || isNaN(Date.parse(dateInput))) {
      throw new BadRequestException('Bad request', { cause: new Error(), description: 'Invalid date format' })
    }

    const lastWeather = await this.locationService.findOne(floatLatitude, floatLongitude, formattedDate)

    return lastWeather
  }
}
