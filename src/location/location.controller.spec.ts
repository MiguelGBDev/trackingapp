import { createMock } from '@golevelup/ts-jest'

import { LocationController } from './location.controller'
import { LocationService } from './location.service'

const LOCATION_COORDINATES = '4.444'
const SERVICE_OBJECT_RESPONSE = {}

describe('LocationController', () => {
  let controller: LocationController
  let locationService: LocationService

  beforeEach(() => {
    locationService = createMock<LocationService>()
    controller = new LocationController(locationService)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  describe('GET getPredictedWeather', () => {
    let serviceFindOneSpy: jest.SpyInstance
    beforeEach(() => {
      serviceFindOneSpy = jest.spyOn(locationService, 'findOne')
    })

    afterEach(() => {
      serviceFindOneSpy.mockClear()
    })

    it('Should return a response for a correct input', async () => {
      serviceFindOneSpy.mockImplementation(() => {
        return SERVICE_OBJECT_RESPONSE
      })
      const response = await controller.getPredictedWeather({
        latitude: LOCATION_COORDINATES,
        longitude: LOCATION_COORDINATES,
        date: new Date().toISOString()
      })

      expect(response).toBe(SERVICE_OBJECT_RESPONSE)
    })

    const wrongInputCases = [
      [undefined, LOCATION_COORDINATES, new Date().toISOString()],
      [LOCATION_COORDINATES, undefined, new Date().toISOString()],
      [LOCATION_COORDINATES, LOCATION_COORDINATES, undefined],
      ['a', LOCATION_COORDINATES, new Date().toISOString()],
      [LOCATION_COORDINATES, 'a', new Date().toISOString()],
      [LOCATION_COORDINATES, LOCATION_COORDINATES, 'a']
    ]

    test.each(wrongInputCases)(
      'input [latitude: %s, longitude: %s, date: %s] should throw BadRequest error: ',
      async (latitude, longitude, date) => {
        await expect(controller.getPredictedWeather({ latitude, longitude, date })).rejects.toThrow('Bad request')
      }
    )
  })
})
