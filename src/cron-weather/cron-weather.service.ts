import { Injectable } from '@nestjs/common'
import { Cron } from '@nestjs/schedule'
import * as dotenv from 'dotenv'

import { LocationRepository } from '../repositories/locationRepository'
import { WeatherRepository } from '../repositories/weatherRepository'
import { ExternalWeatherService } from '../utils/weatherInfo/externalWeatherService'

dotenv.config()
const REFRESH_WEATHER_PERIOD_MINUTES = process.env.REFRESH_WEATHER_PERIOD_MINUTES
const MIN_PERIOD = 5
const MAX_PERIOD = 120

@Injectable()
export class CronWeatherService {
  minutesCounter: number
  period: number

  constructor(
    private readonly locationRepository: LocationRepository,
    private readonly externalWeatherService: ExternalWeatherService,
    private readonly weatherRepository: WeatherRepository
  ) {
    this.minutesCounter = 0

    const number = Number(REFRESH_WEATHER_PERIOD_MINUTES)
    if (!isNaN(number) && number >= MIN_PERIOD && number <= MAX_PERIOD) {
      this.period = number
    } else this.period = MIN_PERIOD
  }

  /**
   * As we are not able to define a cron for specified periods (Cronjobs works with specified defined times)
   * We need to implement a internal mechanism to check the periods between 5 minutes and 2 hours (120 minutes)
   * We set the cronjob to run at every minute as it is the only mutiplier for every possible value defined.
   */
  @Cron(`* * * * *`)
  async handleCron() {
    this.minutesCounter++
    if (this.minutesCounter === this.period) {
      await this.updateWeatherInfo()
      console.info(`[${new Date()}] - Weather info updated`)
      this.minutesCounter = 0
    }
  }

  private async updateWeatherInfo(): Promise<void> {
    const locations = await this.locationRepository.getAll()
    const weatherLocationsInfo = await this.externalWeatherService.getWeatherInfo(locations)
    await this.weatherRepository.saveMany(weatherLocationsInfo)
  }
}
