import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

@Injectable()
export class TrackingRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async getByTrackingNumber(trackingNumber: string) {
    const tracking = await this.prisma.tracking.findUnique({ where: { trackingNumber } })

    return tracking
  }

  async getAll() {
    return await this.prisma.tracking.findMany({})
  }
}
