import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

@Injectable()
export class LocationRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async getById(id: string) {
    const location = await this.prisma.location.findUnique({ where: { id } })
    return location
  }

  async getByCoordinates(latitude: number, longitude: number) {
    const location = await this.prisma.location.findFirst({ where: { latitude, longitude } })
    return location
  }

  async getAll() {
    return await this.prisma.location.findMany({})
  }
}
