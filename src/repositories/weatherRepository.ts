import { Injectable } from '@nestjs/common'
import { PrismaClient } from '@prisma/client'

import { Weather } from '../weather/entities/weather.entity'

@Injectable()
export class WeatherRepository {
  constructor(private readonly prisma: PrismaClient) {}

  async getByLocationId(locationId: string) {
    const weather = await this.prisma.weather.findFirst({
      where: { locationId },
      orderBy: {
        createdAt: 'desc'
      },
      take: 1
    })

    return weather
  }

  async save(weatherDto: Weather) {
    return await this.prisma.weather.create({ data: weatherDto })
  }

  /**
   * Returns the previous weather insertion for the time passed as parameter
   */
  async getLastByLocationIdAndDate(locationId: string, date: Date) {
    return await this.prisma.weather.findFirst({
      where: {
        locationId,
        createdAt: {
          lte: date
        }
      },
      orderBy: {
        createdAt: 'desc'
      },
      take: 1
    })
  }

  async saveMany(weathersDto: Weather[]) {
    return await this.prisma.weather.createMany({ data: weathersDto })
  }
}
